## Write a serial EEPROM

Application presumes a 24FC128 serial EEPROM on the I2C port.
Writes the entire United States Bill of Rights into the first
2663 locations of the EEPROM.  (Text taken from archives.gov).
